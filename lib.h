#include <iostream>
#include <string.h>
#include <vector>
#include <stack>

#define tamCubo 8
#define tamCara 3
#define LOG(x) std::cout << x << std::endl
#define PRINT(x) std::cout << x 

struct nodo
{
    nodo *parent;
    //cubo actual;
    int f,g,h;
};
class cara
{
    public:
        std::string no, ne, so, se;
};

class cubo
{
    public:
        cara up, front, right, back, left, down;
        cubo(cara _up, cara _front, cara _right, cara _back, cara _left, cara _down);
        
    void moveUP(int orientacion)/*+1 sera movimiento horario, -1 movimiento antihorario*/
    {
        std::string aux;
        if (orientacion == 1)
        {
            /*1 pieza*/
            aux = right.no; 
            right.no = back.no;
            back.no = left.no;
            left.no = front.no;
            front.no = aux;
            /*2 pieza*/
            aux = right.ne; 
            right.ne = back.ne;
            back.ne = left.ne;
            left.ne = front.ne;
            front.ne = aux;
            /*piezas frontales*/
            aux = up.no;
            up.no = up.so;
            up.so = up.se;
            up.se = up.ne;
            up.ne = aux;
        }
        else if(orientacion == -1)
        {
            aux = right.no;
            right.no = front.no;
            front.no = left.no;
            left.no = back.no;
            back.no = aux;
            aux = right.ne;
            right.ne = front.ne;
            front.ne = left.ne;
            left.ne = back.ne;
            back.ne = aux;
            aux = up.no;
            up.no = up.ne;
            up.ne = up.se;
            up.se = up.so;
            up.so = aux;
        }
        return;
    };
    void moveFront(int orientacion)
    {
        std::string aux;
        if (orientacion == 1)
        {
            aux = right.no;
            right.no = up.so;
            up.so = left.so;
            left.se = down.ne;
            down.ne = aux;
            aux = right.so;
            right.so = up.se;
            up.se = left.se;
            left.se = down.ne;
            down.ne = aux;
            aux = front.no;
            front.no = front.so;
            front.so = front.se;
            front.se = front.ne;
            front.ne = aux;

        }
        else if(orientacion == -1)
        {
            aux = right.no;
            right.no = down.ne;
            up.so = right.no;
            down.ne = left.se;
            left.se = up.se;
            aux = right.so;
            right.so = down.no;
            down.no = left.ne;
            left.ne = up.se;
            up.se = aux;
            aux = front.no;
            front.no = front.ne;
            front.ne = front.se;
            front.se = front.so;
            front.so = aux;
        }
        return;
    };
    void moveDown(int orientacion)
    {
        std::string aux;
        if(orientacion == 1)
        {
            aux = right.so;
            right.so = front.so;
            front.so = left.so;
            left.so = back.so;
            back.so = aux;
            aux = right.se;
            right.se = front.se;
            front.se = left.se;
            left.se = back.se;
            back.se = aux;
            aux = down.no;
            down.no = down.so;
            down.so = down.se;
            down.se = down.ne;
            down.ne = aux;
        }
        else if(orientacion == -1)
        {
            aux = right.so;
            right.so = back.so;
            back.so = left.so;
            left.so = front.so;
            front.so = aux;
            aux = right.se;
            right.se = back.se;
            back.se = left.se;
            left.se = front.se;
            front.se = aux;
            aux = down.no;
            down.no = down.ne;
            down.ne = down.se;
            down.se = down.so;
            down.so = aux;

        }
        return;
    };
    void moveBack(int orientacion)
    {
        std::string aux;
        if(orientacion == 1)
        {
            aux = right.ne;
            right.ne = down.se;
            down.se = left.so;
            left.so = up.no;
            up.no = aux;
            aux = right.se;
            right.se = down.so;
            down.so = left.no;
            left.no = up.ne;
            up.ne = aux;

            aux = back.no;
            back.no = back.so;
            back.so = back.se;
            back.se = back.ne;
            back.ne = aux;
        }
        else if(orientacion == -1)
        {
            aux = right.ne;
            right.ne = up.no;
            up.no = left.ne;
            left.ne = down.se;
            down.se = aux;
            aux = right.se;
            right.se = up.ne;
            up.ne = left.no;
            left.no = down.so;
            down.so = aux;

            aux = back.no;
            back.no = back.ne;
            back.ne = back.se;
            back.se = back.so;
            back.so = aux;
        }
        return;
    };
    void moveRight(int orientacion)
    {
        std::string aux;
        if(orientacion == 1)
        {
            aux = front.ne;
            front.ne = down.ne;
            down.ne = back.so;
            back.so = up.ne;
            up.ne = aux;
            aux = front.se;
            front.se = down.se;
            down.se = back.no;
            back.no = up.se;
            up.se = aux;

            aux = right.no;
            right.no = right.so;
            right.so = right.se;
            right.se = right.ne;
            right.ne = aux;
        }
        else if(orientacion == -1)
        {
            aux = front.ne;
            front.ne = up.ne;
            up.ne = back.so;
            back.so = down.ne;
            down.ne = aux;
            aux = front.se;
            front.se = up.se; 
            up.se = back.no;
            back.no = down.se;
            down.se = aux;

            aux = right.no;
            right.no = right.ne;
            right.ne = right.se;
            right.se = right.so;
            right.so = aux;
        }
        return;
    };
    void moveLeft(int orientacion)
    {
        std::string aux;
        if (orientacion == 1)
        {
            aux = front.no;
            front.no = up.no;
            up.no = back.se;
            back.se = down.no;
            down.no = aux;
            aux = front.so;
            front.so = up.so;
            up.so = back.ne;
            back.ne = down.so;
            down.so = aux;  

            aux = left.no;
            left.no = left.so;
            left.so = left.se;
            left.se = left.ne;
            left.ne = aux;  
        }
        else if(orientacion == -1)
        {
            aux = front.no;
            front.no = down.no;
            down.no = back.se;
            back.se = up.no;
            up.no = aux;
            aux = front.so;
            front.so = down.so;
            down.so = back.ne;
            back.ne = up.so;
            up.so = aux;

            aux = left.no;
            left.no = left.ne;
            left.ne = left.se;
            left.se = left.so;
            left.so = aux;
        }
        return;
    };
    
};
cubo::cubo(cara _up, cara _front, cara _right, cara _back, cara _left, cara _down)
{
    up = _up;
    front = _front;
    right = _right;
    left = _left;
    back = _back;
    down = _down;
    return;
}
void print(cubo _cubo)
{
    PRINT(_cubo.up.no);
    PRINT(_cubo.up.ne);
    PRINT(_cubo.up.so);
    PRINT(_cubo.up.se);
    LOG("-up");
    PRINT(_cubo.front.no);
    PRINT(_cubo.front.ne);
    PRINT(_cubo.front.so);
    PRINT(_cubo.front.se);
    LOG("-front");
    PRINT(_cubo.right.no);
    PRINT(_cubo.right.ne);
    PRINT(_cubo.right.so);
    PRINT(_cubo.right.se);
    LOG("-right");
    PRINT(_cubo.back.no);
    PRINT(_cubo.back.ne);
    PRINT(_cubo.back.so);
    PRINT(_cubo.back.se);
    LOG("-left");
    PRINT(_cubo.left.no);
    PRINT(_cubo.left.ne);
    PRINT(_cubo.left.so);
    PRINT(_cubo.left.se);
    LOG("-back");
    PRINT(_cubo.down.no);
    PRINT(_cubo.down.ne);
    PRINT(_cubo.down.so);
    PRINT(_cubo.down.se);
    LOG("-down");
}
int comparaCara(cara c0, cara c1)
/*Hay que devolver el numero de fichas que no coinciden con su posicion correcta,
es decir que devolvemos la h para el algoritmo.*/
{
    if(c0.no != c1.no)
    {
        if(c0.ne != c0.ne)
        {
            if(c0.se != c1.se)
            {
                if(c0.so != c1.so)
                    return 4;
                return 3;
            }
            return 2;
        }
        return 1;
    }
    return 0;
}

int comparaCubo(cubo c0, cubo c1)
{
    int h = 0;
    h += comparaCara(c0.up, c1.up);
    h += comparaCara(c0.front, c1.front);
    h += comparaCara(c0.left, c1.left);
    h += comparaCara(c0.right, c1.right);
    h += comparaCara(c0.back, c1.back);
    h += comparaCara(c0.down, c1.down);
    return h;
}