#include "lib.h"
/*
    PARA NUESTRO ALGORITMO HEMOS USADO LA HEURISTICA DE: CANTIDAD DE PIEZAS DESCOLOCADAS.
    ASI QUE NUESTRO ARRAY DE ABIERTOS Y CERRADOS SERA EN FUNCION DE ESA H
*/
int main(int argc, char*argv[])
{   
    std::vector<struct nodo> abierta, cerrada;
    std::stack<nodo> pila;
    
    /*
    REGLAS: cubo.moveUP, cubo.moveBack, cubo.moveDown,
    cubo.moveFront, cubo.moveLeft, cubo.moveRight.
    */

    /*el maximo de movimientos que nos pide el enunciado es de 5, asi que no tendremos que hacer mas.*/
    //abierta = std::vector<struct nodo>(5);
    //abierta.reserve(5);
    //abierta.insert(1) = 5;
    /*abierta[1].h = 4;
    abierta[2].h = 3;
    abierta[3].h = 2;
    abierta[4].h = 1;
    abierta[5].h = 0;*/

    cara _up, _down, _right, _left, _back, _front;
    LOG("Introduzca de izq a derecha y de arriba abajo los valores para la cara UP: ");
    std::cin >> _up.no >> _up.ne >> _up.so >> _up.se;
    LOG("Introduzca de izq a derecha y de arriba abajo los valores para la cara FRONT: ");
    std::cin >> _front.no >> _front.ne >> _front.so >> _front.se;
    LOG("Introduzca de izq a derecha y de arriba abajo los valores para la cara RIGHT: ");
    std::cin >>_right.no >> _right.ne >> _right.so >> _right.se;
    LOG("Introduzca de izq a derecha y de arriba abajo los valores para la cara LEFT: ");
    std::cin >> _left.no >> _left.ne >> _left.so >> _left.se;
    LOG("Introduzca de izq a derecha y de arriba abajo los valores para la cara BACK: ");
    std::cin >> _back.no >> _back.ne >> _back.so >> _back.se;
    LOG("Introduzca de izq a derecha y de arriba abajo los valores para la cara DOWN: ");
    std::cin >> _down.no >> _down.ne >> _down.so >> _down.se;
    LOG("Posicion inicial del cubo: ");
    cubo miCubo = cubo(_up,_front,_right,_back,_left,_down);
    _up.no = "w1"; _up.ne = "w2"; _up.so = "w3"; _up.se = "w4";
    _front.no = "b1"; _front.ne = "b2"; _front.so = "b3"; _front.se = "b4";
    _right.no = "o1"; _right.ne = "o2"; _right.so = "o3"; _right.se = "o4";
    _back.no = "g1"; _back.ne = "g2"; _back.so = "g3"; _back.se = "g4"; 
    _left.no = "r1"; _left.ne = "r2"; _left.so = "r3"; _left.se = "r4";
    _down.no = "y1"; _down.ne = "y2"; _down.so = "y3"; _down.se = "y4";
    cubo cuboIdeal = cubo(_up,_front,_right,_back,_left,_down);
    print(miCubo);
    print(cuboIdeal);
    LOG("");
    PRINT("Hinicial = ");
    int h = comparaCubo(miCubo, cuboIdeal);
    PRINT(h);
    LOG("");
    /*una vez que sabemos cual es la heuristica de nuestro cubo,
    tenemos que empezar a simular el arbol de movimientos
    para saber cual es el movimiento que debemos aplicar
    que produzca un avance en el algoritmo.*/
    /*Creamos un nuevo cubo, copia del introducido por el usuario
    sobre el que haremos la simulacion de movimientos
    y como resultado obtendremos la heuristica de la simulacion,
    para ello llamaremos a comparaCubo(miCubo, cuboSimulado);*/
    cubo simulaCubo = miCubo;
    /*Las reglas a aplicar en nuestros cubos seran el conjunto de reglas
    sobre el que el sistema debe decidir si aplica o no aplica.*/
    while(h != 0)
    {
        simulaCubo.moveUP(1);
        print(simulaCubo);
        PRINT("Hactual = ");
        PRINT(comparaCubo(miCubo, simulaCubo));
        LOG("");
    }
    LOG("Compeltado!.");
    return 0;
}
